<?php

session_start();

//report any error
error_reporting(E_ALL); ini_set('display_errors', 1); mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

//connect to db
include 'db_connect.php';

//recaptcha verification
if (isset($_POST["g-recaptcha-response"])) {

	$response = $_POST["g-recaptcha-response"];
	$url = 'https://www.google.com/recaptcha/api/siteverify';
	$data = array(
		'secret' => '6LdmfikUAAAAALCSClhw0yIuB1K0MhtijMZv217s', //test: 6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe
		'response' => $_POST["g-recaptcha-response"]
		);
	$options = array(
		'http' => array (
			'header' => "Content-type: application/x-www-form-urlencoded\r\n",
			'method' => 'POST',
			'content' => http_build_query($data)
			)
		);
	$context  = stream_context_create($options);
	$verify = file_get_contents($url, false, $context);
	$captcha_success=json_decode($verify);

	if ($captcha_success->success==false) {
		//echo "<p>You are a bot! Go away!</p>";
		echo "<script type='text/javascript'>";
		echo "alert('To continue, please make sure you are not a robot.');";
		echo "window.location.href='userinfo.php';";
		echo "</script>";
	} else if ($captcha_success->success==true) {
		echo "<p>You are not a bot!</p>";

		//submit form
		if(isset($_POST['submit'])) {


			//php validation if html validation failed
			if(!isset($_SESSION['consent-rd']) || !isset($_SESSION['Rddcollection']) || !isset($_POST['fullname']) || !isset($_POST['mykad']) || !isset($_POST['phone_num']) || !isset($_POST['email']) || (($_SESSION['goldQty']=='0' && $_SESSION['silverqty']=='0') && $_SESSION['setqty']=='0') ) {
				//terminate submission
				header("Location: index.php");
				die('Please enter the required fields.');
			} else {
				$fullname = stripslashes($_POST['fullname']);
				$fullName = strtoupper($fullname);

				if(!isValidName($fullName)) {
					die("Please enter a valid name.");
				}

				$mykad = stripslashes($_POST['mykad']);
				$mykad = strtoupper(mysqli_real_escape_string($conn, $mykad));

				if(!isValidMykad($mykad)) {
					die("Please enter a valid mykad or passport number without dash.");
				}

				$phone_num = stripslashes($_POST['phone_num']);
				$phone_num = mysqli_real_escape_string($conn, $phone_num);
				
				if(!isValidPhone($phone_num)) {
					die("Your phone number should be between 9 - 10 digits including area codes and without dash.");
				}

				$email = stripslashes($_POST['email']);
				$email = mysqli_real_escape_string($conn, $email);

				if(!isValidEmail($email)) {
					die("Your email is invalid");
				}

				$collect_pt = $_SESSION['Rddcollection'];

				date_default_timezone_set('Asia/Kuala_Lumpur');
				$order_dt = date('d/m/Y h:i:s a', time()); //TODO: remote mysql order_dt is different than time zone order dt

				if($_SESSION['consent-rd'] == "agree") {
					$pdpa_consent = "Y";
				} else {
					$pdpa_consent = "N";
				}

				$gold_coin = $_SESSION['goldQty'];
				$silver_coin = $_SESSION['silverqty'];
				$set_coin = $_SESSION['setqty'];

				//create session
				$_SESSION['fullname'] = $fullName;
				$_SESSION['mykad'] = $mykad;
				$_SESSION['phone_num'] = $phone_num;
				$_SESSION['email'] = $email;
				$_SESSION['order_dt'] = $order_dt;

				//insert order details to DB
				$insertToDB = "INSERT INTO `coinorder` (fullname, mykad, phone_num, email, gold_coin, silver_coin, set_coin, collect_pt, pdpa_consent)
				VALUES (?,?,?,?,?,?,?,?,?)";

				if($statementIns = $conn->prepare($insertToDB)) {
					$statementIns->bind_param('ssisiiiss',$fullName, $mykad, $phone_num, $email, $gold_coin, $silver_coin, $set_coin, $collect_pt, $pdpa_consent);
					$statementIns->execute();
					$statementIns->close();
					echo "Submission is successful";
			header("Refresh: 1; url= orderDetails.php"); //TODO: generate order details to be print out or save
		} else {
			echo "Unsuccessful submission. Please try again.";
			header("Refresh: 1; url= index.php");
		}

	} //if all fields are filled in

} //if submit
$conn->close();
}
} else { //if human
	die('Please make sure you are human!');
}

function isValidName($name) {
	return preg_match('/^[a-zA-Z\'\-\040]+$/',$name);
}

function isValidMykad($mykad) {
	return preg_match('/^[a-zA-Z0-9]+$/',$mykad);
}

function isValidEmail($email) {
	return filter_var($email, FILTER_VALIDATE_EMAIL) && preg_match ('/@.+\./', $email);
}

function isValidPhone($phone) {
	return preg_match('/^[0-9]{9,10}$/', $phone);
}

?>