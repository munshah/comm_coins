<?php
session_start();

//Validate that the user agree to T&C-->
if(isset($_POST['consent-rd']) && $_POST['consent-rd'] === 'agree') {
	$_SESSION['consent-rd'] = $_POST['consent-rd'];
} else if (isset($_POST['consent-rd']) && $_POST['consent-rd'] === 'disagree') {
  echo "<script type='text/javascript'>";
  echo "alert('Please read and agree with the terms and conditions in the Personal Data Protection Act.');";
  echo "window.location.href='index.php';";
  echo "</script>";
} else if(!isset($_SESSION['consent-rd']) || !isset($_POST['consent-rd'])) {
  header("Location: index.php");
}

header("Cache-Control: private, must-revalidate, max-age=0");
header("Pragma: no-cache");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // A date in the past

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>BANK NEGARA MALAYSIA</title>
  <!-- Custom CSS -->
  <link rel ="stylesheet" type="text/css" href="css/main.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

</head>

<body>
  <form id="order-form" name="order-form" method="post" onsubmit="return validate(this);" action="userinfo.php" >
    <p>&nbsp;</p>
    <table width="1299" border="0">
      <tr>
        <td width="129">&nbsp;</td>
        <td width="1063"><div align="center">
          <table width="1033" border="1">
            <tr>
              <td width="1023" height="64" bgcolor="#000066">BANK NEGARA MALAYSIA - Commemorative Coin Order Form</td>
            </tr>
            <tr>
              <td height="118"><table width="1025" border="0">
                <tr>
                  <td width="1019" bgcolor="#FFFFFF"><p>Please select the item(s) you would like to order and choose location of collection.</p>
                    <div align="center">
                      <table width="200" border="1">
                        <tr bgcolor="#BEDFF1">
                          <td bgcolor="#F0F0FF">DESIGN OF COMMEMORATIVE COINS IN CONJUCTION WITH 50TH ANNIVERSARY OF YAYASAN SABAH</td>
                        </tr>
                        <tr>
                          <td><div align="center">
                            <table width="677" border="0">
                              <tr>
                                <td bgcolor="#FFFFFF"><table width="672" border="0">
                                  <tr>
                                    <td width="38">&nbsp;</td>
                                    <div align="center"><img src="img/coin.jpeg" /></div>

                                    <td width="41">&nbsp;</td>
                                  </tr>
                                </table></td>
                              </tr>
                            </table>
                          </div></td>
                        </tr>
                      </table>
                      <table width="692" border="0">
                        <tr>
                          <td width="686" bgcolor="#FFFFFF"><p>What is the item you would like to order ?
                          </p></td>
                        </tr>
                        <tr>
                          <td height="319" bgcolor="#FFFFFF"><table width="678" border="0">
                            <tr>                            </tr>
                          </table>
                          <table width="683" border="1">
                            <tr>
                              <td width="393" bgcolor="#F0F0FF"><table width="366" border="0">
                                <tr>
                                  <td width="360">Type</td>
                                </tr>
                              </table></td>
                              <td width="274" bgcolor="#F0F0FF"><table width="265" border="0">
                                <tr>
                                  <td width="259">Quantity</td>
                                </tr>
                              </table></td>
                            </tr>
                            <tr>
                              <td>Coloured Silver Commemorative Coin (proof)</td>
                              <td><div align="center">
                                <label for="silverqty"></label>
                                <select name="silverqty" id="silverqty">
                                  <option value="0">(NONE)</option>
                                  <option value="1">1</option>
                                  <option value="2">2</option>
                                  <option value="3">3</option>
                                  <option value="4">4</option>
                                  <option value="5">5</option>
                                </select>
                              </div></td>
                            </tr>
                            <tr>
                              <td>Nordic Gold Brilliant Uncirculated (B.U) Commemorative Coin </td>
                              <td><div align="center">
                                <select name="goldQty" id="goldQty">
                                  <option value="0">(NONE)</option>
                                  <option value="1">1</option>
                                  <option value="2">2</option>
                                  <option value="3">3</option>
                                  <option value="4">4</option>
                                  <option value="5">5</option>
                                </select>
                              </div></td>
                            </tr>
                            <tr>
                              <td>Set of 2 </td>
                              <td><div align="center">
                                <select name="setqty" id="setqty">
                                  <option value="0">(NONE)</option>
                                  <option value="1">1</option>
                                  <option value="2">2</option>
                                  <option value="3">3</option>
                                  <option value="4">4</option>
                                  <option value="5">5</option>
                                </select>
                              </div></td>
                            </tr>
                          </table>
                          <p>Collection Point </p>
                          <table width="511" border="0">
                            <tr>
                              <td width="505">
                                <label>
                                  <input type="radio" name="Rddcollection" required value="BNM HQ - KL" id="Rddcollection_0" />
                                  BNM HQ - KL</label>
                                  <br />
                                  <label>
                                    <input type="radio" name="Rddcollection" value="BNM Pulau Pinang" id="Rddcollection_1" />
                                    BNM Pulau Pinang</label>
                                    <br />
                                    <label>
                                      <input type="radio" name="Rddcollection" value="BNM Kuala Terengganu" id="Rddcollection_2" />
                                      BNM Kuala Terengganu</label>
                                      <br />
                                      <label>
                                        <input type="radio" name="Rddcollection" value="BNM Johor Bahru" id="Rddcollection_3" />
                                        BNM Johor Bahru</label>
                                        <br />
                                        <label>
                                          <input type="radio" name="Rddcollection" value="BNM Kota Kinabalu" id="Rddcollection_4" />
                                          BNM Kota Kinabalu</label>
                                          <br />
                                          <label>
                                            <input type="radio" name="Rddcollection" value="BNM Kuching" id="Rddcollection_5" />
                                            BNM Kuching</label>
                                            <br /></td>
                                          </tr>
                                        </table></td>
                                      </tr>
                                      <tr>
                                        
                                        <td width="165">
                                          <table width="116" height="32">
                                            <tr>
                                              <input type="submit" name="submit-order" class="submit-btn" value="NEXT"  >
                                            </tr>
                                          </table></td>
                                          <td width="133">&nbsp;</td>
                                        </tr>
                                      </table>
                                      <table width="501" border="0">
                                        <tr> </tr>
                                      </table></td>
                                    </tr>
                                  </table>
                                </div></td>	
                              </tr>
                            </table></td>
                          </tr>
                        </table>
                      </div></td>
                      <td width="93">&nbsp;</td>
                    </tr>
                  </table>
                </form>

                <!-- Validate that selection of coin is made -->
                <script type="text/javascript">
                  function validate(form) {
                    var e = document.getElementById("silverqty");
                    var silverqty = e.options[e.selectedIndex].value;

                    var a = document.getElementById("goldQty");
                    var goldQty = a.options[a.selectedIndex].value;

                    var b = document.getElementById("setqty");
                    var setqty = b.options[b.selectedIndex].value;

                    if(silverqty != "0") {
                     coin_type = "Silver Coin - " + silverqty ;
                   } else {
                     coin_type = "";
                   }

                   if(goldQty != "0" && coin_type != "") {
                     coin_type += ", Gold Coin - " + goldQty;
                   } else if(goldQty != "0" && coin_type == ""){
                     coin_type = "Gold Coin - " + goldQty ;
                   }

                   if(setqty != "0" && coin_type != "") {
                     coin_type += ", Set of two - " + setqty;
                   } else if(setqty != "0" && coin_type == ""){
                     coin_type = "Set of two - " + setqty;
                   }	

                   if((silverqty==0 && goldQty==0) && setqty==0) {
                    alert("Please select the type of coin to purchase.");
                    return false;
                  } else {
                   return confirm("You order " + coin_type + ". Is this your final order?");
                 }
               }
             </script>


           </body>
           </html>