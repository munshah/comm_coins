<?php
session_start();

header("Cache-Control: private, must-revalidate, max-age=0");
header("Pragma: no-cache");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // A date in the past
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>BANK NEGARA MALAYSIA</title>
  <!-- Custom CSS -->
  <link rel ="stylesheet" type="text/css" href="css/main.css">
</head>

<body>

  <form id="consent-form" name="consent-form" method="post" action="order.php">
    <table width="1308" border="0">
      <tr>
        <td width="1302" height="134"><div align="center">
          <table width="1045" border="1">
            <tr>
              <td width="1035" height="64" bgcolor="#000066">BANK NEGARA MALAYSIA - Commemorative Coin Order Form</td>
            </tr>
            <tr>
              <td height="118" bgcolor="#FFFFFF"><table width="688" border="0">
                <tr>
                  <td height="71"><p>General Information</p>
                    <table width="200" border="0">
                      <tr>
                        <td>*Required</td>
                      </tr>
                    </table></td>
                  </tr>
                </table>
                <table width="740" border="0">
                  <tr>
                    <td width="734"><p>PDPA - Please read and agree with the terms and conditions in the Personal Data Protection Act</p>
                      <div id="consent-apDiv1">*</div>
                      <p>
                        <input type="radio" name="consent-rd" id="agree" required value="agree" />
                        <label for="rdbagree">Agree</label>
                      </p>
                      <p>
                        <input type="radio" name="consent-rd" id="disagree" value="disagree" />
                        <label for="rdbdisagree">Disagree</label>
                      </p></td>
                    </tr>
                  </table>
                  <table width="745" border="0">
                    <tr>
                      <td colspan="2"><img src="img/disclaimer.png" alt="" width="213" height="48" /></td>
                    </tr>
                    <tr>
                      <td width="36">&nbsp;</td>
                      <td width="699">
                        <ol>
                          <li>Submission of this order does not guarantee allocation of commemorative coin. Sale is based on ballot process.
                            <li>Successful orders will ne notified via email on the confirmation of allocated item, collection date and payment information.
                              <li>All rpices inclusive of 6% Goods and Services Tax (GST)
                                <li>Uncollected balloted orders will be sold to others through subsequent ballot process.
                                </ol>
                              </td>
                            </tr>
                            <tr>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                            </tr>
                            <tr>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                            </tr>
                          </table>
                          <div align="center"></div></td>
                        </tr>
                      </table>
                    </div></td>
                  </tr>
                  <tr>
                    <td height="37"><div align="center">
                      <table width="696" border="0">
                        <tr>
                          <td width="62">&nbsp;</td>
                          <td width="486">&nbsp;</td>
                          <td width="134"><table width="135">
                            <tr>
                              <input type="submit" name="submit-consent" class="submit-btn" value="NEXT" />
                            </tr>
                          </table></td>
                        </tr>
                      </table>
                    </div></td>
                  </tr>
                </table>
                <div align="center"></div>
              </form>

            </body>
            </html>