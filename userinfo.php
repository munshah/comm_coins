<?php
session_start();

if(isset($_POST['silverqty'])) {
	$_SESSION['silverqty'] = $_POST['silverqty'];
}

if(isset($_POST['goldQty'])) {
	$_SESSION['goldQty'] = $_POST['goldQty'];
}

if(isset($_POST['setqty'])) {
	$_SESSION['setqty'] = $_POST['setqty'];
}

if(isset($_POST['Rddcollection'])) {
	$_SESSION['Rddcollection'] = $_POST['Rddcollection'];
}

header("Cache-Control: private, must-revalidate, max-age=0");
header("Pragma: no-cache");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // A date in the past

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>BANK NEGARA MALAYSIA</title>
  <!-- Custom CSS -->
  <link rel ="stylesheet" type="text/css" href="css/main.css">
  <script src='https://www.google.com/recaptcha/api.js'></script>
</head>

<body>


  <form id="user-form" name="user-form" method="post" action="submission.php">
    <p>&nbsp;</p>
    <table width="1305" border="0">
      <tr>
        <td width="195">&nbsp;</td>
        <td width="911"><table width="1045" border="1">
          <tr>
            <td width="1035" height="64" bgcolor="#000066">BANK NEGARA MALAYSIA - Commemorative Coin Order Form</td>
          </tr>
          <tr>
            <td height="54" bgcolor="#FFFFFF"><table width="1038" border="0">
              <tr>
                <td width="1032" bgcolor="#000066">Contact Information</td>
              </tr>
            </table>
            <table width="1039" border="0">
              <tr>
                <td width="1033" height="191"><table width="981" border="0">
                  <tr>
                    <td width="336" height="32" bgcolor="#CCDDEA">Full Name (as per MyKad / Passport)
                      <div id="user-apDiv2">*</div></td>
                      <td width="561">
                        <label for="text1"></label>
                        <input type="text" name="fullname" id="fullname" pattern="[A-Za-z\s]+" required />
                        <td width="70">&nbsp;</td>
                      </tr>
                      <tr>
                        <td height="27" bgcolor="#CCDDEA">MyKad Number / Passport Number </td>
                        <td><input type="text" name="mykad" id="mykad" pattern="[A-Za-z0-9]+" required /></td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td height="26" bgcolor="#CCDDEA">Phone Number
                          <div id="user-apDiv3">*</div></td>
                          <td><input type="text" name="phone_num" id="phone_num" placeholder="e.g 0122498290" pattern="[0-9]{9,10}$" required /></td>
                          <td>&nbsp;</td>	
                        </tr>
                        <tr>
                          <td height="25" bgcolor="#CCDDEA">E-mail
                            <div id="user-apDiv">*</div></td>
                            <td><input type="email" name="email" id="email" placeholder="e.g abcd@gmail.com" pattern="[a-z0-9._%+-]+@[a-z0-9._%+-]+\.[a-z]{2,3}+\.[a-z]{2,3}$" required /></td>
                            <td>&nbsp;</td>
                          </tr>
                        </table>
                        <div id="user-apDiv4">*</div>
                        <table width="679" border="0">
                          <tr>
                            <td width="246" height="39">&nbsp;</td>
                            <td>
                              <tr>
                                <!-- test: 6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI -->
                                <div class="g-recaptcha" data-sitekey="6LdmfikUAAAAAN8LpiFBLYc-HJV_trBYI5-R_qn8"></div>
                              </tr>
                            </td>

                            <td width="165"><table width="116" height="32">
                              <tr>
                                <input type="submit" class="submit-btn" name="submit" value="NEXT">
                              </tr>
                            </table></td>
                            <td width="133">&nbsp;</td>
                          </tr>
                        </table>
                        <p>&nbsp;</p></td>
                      </tr>
                    </table></td>
                  </tr>
                </table></td>
                <td width="185">&nbsp;</td>
              </tr>
            </table>
          </form>
        </body>
        </html>